package com.example.numberguessinggame

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private var points = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        pickRandomNumbers()
    }

    fun leftButtonClick(v: View) {
        checkIfCorrectAnswer(true)
    }

    fun rightButtonClick(v: View) {
        checkIfCorrectAnswer(false)
    }

    private fun checkIfCorrectAnswer(isLeft: Boolean) {
        val leftNum = left_button.text.toString().toInt()
        val rightNum = right_button.text.toString().toInt()
        if (isLeft && leftNum > rightNum || !isLeft && rightNum > leftNum) {
            points++
            Toast.makeText(this, "Correct answer! You have won 1 point.", Toast.LENGTH_SHORT).show()
        } else {
            points--
            Toast.makeText(this, "Correct answer! You have lost 1 point.", Toast.LENGTH_SHORT)
                .show()
        }
        pointstText.text = "Points: $points"
        pickRandomNumbers()
    }

    private fun pickRandomNumbers() {
        val r = Random()
        val num1 = r.nextInt(10)
        val num2 = r.nextInt(10)
        if (num1 == num2) {
            pickRandomNumbers()
        }
        left_button.text = "$num1"
        right_button.text = "$num2"
    }
}